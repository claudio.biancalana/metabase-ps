FROM metabase/metabase:v0.43.4
COPY metabase_modlink.jar /app/metabase.jar

EXPOSE 3000

ENTRYPOINT ["/app/run_metabase.sh"]